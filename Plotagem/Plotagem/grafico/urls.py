from django.urls import path, include
from . import views
from .views import *

urlpatterns = [
    path('', views.index),
    path('login/', views.login, name='login'),
    path('painel/', views.painel, name='painel'),
    path('graph/', TesteView.as_view(), name='graph'),
    path('painel/graph/', TesteView.as_view(), name='graph'),
    path('graph2/', TesteView2.as_view(), name='graph2'),
    path('painel/graph2/', TesteView2.as_view(), name='graph2'),
    path('test/', views.test, name='test'),
]
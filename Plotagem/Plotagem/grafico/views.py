from django.shortcuts import render
from django.views.generic import TemplateView
from .models import *


def index(request):
    return render(request, 'index.html')


def login(request):
    return render(request, 'login.html')


def painel(request):
    return render(request, 'painel.html')


def test(request):
    return render(request, 'test.html')


class TesteView(TemplateView):
    template_name = "plot.html"

    def get_context_data(self, **kwargs):
        context = super(TesteView, self).get_context_data(**kwargs)
        context['graph'] = teste()
        return context


class TesteView2(TemplateView):
    template_name = "plot2.html"

    def get_context_data(self, **kwargs):
        context2 = super(TesteView2, self).get_context_data(**kwargs)
        context2['graph2'] = teste2()
        return context2
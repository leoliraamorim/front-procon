import numpy as np
import plotly.graph_objs as go
#import plotly.offline as plot
from plotly.offline import plot



def teste():
    n = 201
    x = np.linspace(0, 2.0*np.pi, n)
    y1 = np.sin(x)

    trace1 = go.Scatter(
        x=x,
        y=y1,
        name="sine curve",
        line=dict(
            width=4,
            dash='dash'
        )
    )

    layout = dict(
        title="Gráfico de teste 01",
        autosize=False,
        width=450,
        height=300,
        xaxis=dict(title="dimensão x"),
        yaxis=dict(title="dimensão y"),
        paper_bgcolor='#fafafa',
        plot_bgcolor='#fafafa',
    )

    data = [trace1]

    fig = dict(data=data, layout=layout)
    plot_div = plot(fig, output_type='div', include_plotlyjs=False, auto_open=True)
    return plot_div


def teste2():
    n = 201
    x = np.linspace(0, 2.0*np.pi, n)
    y1 = np.sin(x)

    trace1 = go.Scatter(
        x=x,
        y=y1,
        name="sine curve",
        line=dict(
            width=4,
            dash='dash',
            color='red',
        )
    )

    layout = dict(
        title="Gráfico de teste 02",
        autosize=False,
        width=450,
        height=300,
        xaxis=dict(title="dimensão x"),
        yaxis=dict(title="dimensão y"),
        paper_bgcolor='#fafafa',
        plot_bgcolor='#fafafa',
    )

    data = [trace1]

    fig = dict(data=data, layout=layout)
    plot_div = plot(fig, output_type='div', include_plotlyjs=False, auto_open=True)
    return plot_div
